[IOT STUDENT HOME](https://gitlab.com/iot110/iot110-student/blob/master/README.md)

## Setting up Lab8

**Synopsis:** For this lab we will study gateways and examine the Message Queue
Telemetry Transport (MQTT) protocol in preparation for our last class where we 
create a connection to an instructor led Microsoft Azure IoT Suite connection.

### Objectives
* Install MQTT Message Broker
* Install the MQTT Client Tool (MQTT.fx)
* Create MQTT Test code, Sensor Sim and Update SenseHat Web main.py
* Testing Multiple Pub/Sub Connections

The various steps of this lab are summarized as:
* [PART A](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab8/PartA.md) Install MQTT Message Broker
* [PART B](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab8/PartB.md) Install MQTT Client Tool MQTT.fx
* [PART C](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab8/PartC.md) Create MQTT Python Test Code
* [PART D](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab8/PartD.md) Test Multiple Pub/Sub Connections

[IOT STUDENT HOME](https://gitlab.com/iot110/iot110-student/blob/master/README.md)
