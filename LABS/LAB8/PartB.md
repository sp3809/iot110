[LAB8 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab8/setup.md)

## Part B - Install MQTT Client Tool MQTT.fx
**Synopsis:** In this part, we will install and test the MQTT.fx client test tool.
This tool is quite useful in troubleshooting connections to the MQTT broker and
is able to explore the operation of publishing and subscribing to "topics".

## Objectives
* Download an [MQTT Test Client](http://www.jensd.de/apps/mqttfx/)
* Install and Configure MQTT.fx on development host
* Connect to our Mosquitto Broker (Server)

### Step B1: Install and Configure MQTT.fx
The following show installation on MacOSX, but a similar install and configuration
should apply to whatever is the target host development platform being used
for development in this class.
![MQTT Connection](https://gitlab.com/iot110/iot110-student/raw/master/Resources/Images/MQTTfx_Connect.png)

### Step B2: Subscribe to some topics
![Subscribe-Redirect](https://gitlab.com/iot110/iot110-student/raw/master/Resources/Images/SubscribeRedirect.png)

### Step B3: Publish to some topics
![First Publish](https://gitlab.com/iot110/iot110-student/raw/master/Resources/Images/MQTT_FirstPub.png)

### Step B4: Observe Subscribed Topics
![Subscribed Topics](https://gitlab.com/iot110/iot110-student/raw/master/Resources/Images/SubscribeHello.png)


[PART C](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab8/PartC.md) Create MQTT Python Test Code

[LAB8 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab8/setup.md)
